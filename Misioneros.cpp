#include <cstdlib>
#include <iostream>
#include <stack>
#include <stdio.h>
using namespace std;
int  Misioneros [2], Canibales [2],Bote [2];
int pro=0;
stack <int> MisionerosI,MisionerosD, CanibalesI,CanibalesD, BoteI, BoteD; 


void solucionar(int M[], int C[], int  bote[], int pro);
void pila();
int main(int argc, char *argv[]){
	Misioneros[0] = 3;
	Misioneros[1] = 0;
	Canibales [0] = 3;
	Canibales[1]=0;
	Bote[0]=1;
	Bote[1]=0;
	MisionerosI.push(Misioneros[0]);
	MisionerosD.push(Misioneros[1]);
	CanibalesI.push(Canibales[0]);
	CanibalesD.push(Canibales[1]);
	BoteI.push(Bote[0]);
	BoteD.push(Bote[1]);
	solucionar( Misioneros, Canibales, Bote, pro);
	pila();
	
	//cout << "Canibales";    
	return 0;
}

void solucionar(int M[], int C[], int  bot[], int pro){	
	/*Reglas */	
	/*Mover un Misionero y un canibal de derecha a izquierda*/
	if(bot[1]==1 && (M[0]++)<C[0] && M[1]>0 && M[0]<3 && C[1]>0 && C[0]<3 && pro<11){		
		M[1]--;
		M[0]++;
		C[1]--;
		C[0]++;
		bot[1]--;
		bot[0]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}				
	}	
	/*
	Mover dos Misioneros del lado derecho al lado izquierdo
	*/
	if(bot[1]==1 && ((M[0])+2)<C[0] && M[1]>=2 && M[0]<=1 && pro<11){	
		M[1]= M[1]-2;
		M[0]= M[0]+2;
		bot[1]--;
		bot[0]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
	/*
		Mover dos Canibales del lado derecho al lado izquierdo
	*/	
	if(bot[1]==1 && C[1]>=2 && C[0]<=1 && pro<11){	
		C[1]=C[1]-2;
		C[0]=C[0]+2;	
		bot[1]--;
		bot[0]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}	
	/*
		Mover un Misionero y un canibal del lado izquierdo al lado derecho	
	*/
	if(bot[0]==1 && (M[1]++)<C[1] && M[0]>=1 && M[1]<3 && C[0]>=1 && C[1]<3 && pro<11){	
		C[0]--;
		M[0]--;
		C[1]++;
		M[1]++;
		bot[0]--;
		bot[1]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
	/*
	Mover dos Misioneros del lado izquierdo al derecho
	*/
	if(bot[0]==1 && (M[1]+2)<C[1] && M[0]>=2 && M[1]<2 && pro<11){	
		M[0]=M[0]-2;
		M[1]=M[1]+2;
		bot[0]--;
		bot[1]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
	/*Mover dos Canibales del lado izquierdo al derecho*/
	if(bot[0]==1 && C[0]>=2 && C[1]<2 && pro<11){		
		C[0]=C[0]-2;
		C[1]=C[1]+2;
		bot[0]--;
		bot[1]++;	
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
	/*Mover un Misionero del lado derecho al izquierdo*/	
	if(bot[1]==1 && (M[0]++)<C[0] && (M[0])<3 && (M[1])>1 && pro<11)
	{
		M[1]--;
		M[0]++;
		bot[1]--;
		bot[0]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
	/*Mover un Misionero del lado izquierdo al derecho*/
	if(bot[0]==1 && (M[1]++)<C[1] && M[0]>=1 && M[1]<3 && pro<11){	
		M[0]--;
		M[1]++;
		bot[0]--;
		bot[1]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
	/*Mover un canibal del lado derecho al izquierdo*/
	if(bot[1]==1 && C[1]>=1 && C[0]<3 && pro<11){			
		C[1]--;
		C[0]++;	
		bot[1]--;
		bot[0]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3 && pro<11){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
	/*Mover un canibal del lado izquierdo al derecho*/
	if(bot[0]==1 && C[0]>=1 && C[1]<3 && pro<11){		
		C[0]--;
		C[1]++;
		bot[0]--;
		bot[1]++;
		pro++;
		MisionerosI.push(M[0]);
		MisionerosD.push(M[1]);
		CanibalesI.push(C[0]);
		CanibalesD.push(C[1]);
		BoteI.push(bot[0]);
		BoteD.push(bot[1]);
		solucionar(M, C,bot, pro);
		if(MisionerosD.top()!=3 && CanibalesD.top() !=3 && pro<11){
			MisionerosD.pop();
			CanibalesD.pop();
			MisionerosI.pop();
			CanibalesI.pop();
			BoteI.pop();
			BoteD.pop();
		}
	}
		
}


void pila(){
	int i;
	while(!MisionerosI.empty()){
		printf("%d - %d - %d - %d - %d - %d \n",CanibalesI.top(),CanibalesD.top(),MisionerosI.top(),MisionerosD.top(),BoteI.top(),BoteD.top() );
		MisionerosI.pop();
		MisionerosD.pop(); 
		CanibalesI.pop();
		CanibalesD.pop(); 
		BoteI.pop(); 
		BoteD.pop();		
	}
}
